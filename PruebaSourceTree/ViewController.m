//
//  ViewController.m
//  PruebaSourceTree
//
//  Created by JMM on 19/01/13.
//  Copyright (c) 2013 JMM. All rights reserved.
//

#import "ViewController.h"
#import <Parse/Parse.h>


@interface ViewController ()

@end

@implementation ViewController
@synthesize muestraNumeros, campoApellido, campoNombre;



- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated..........
}

- (IBAction)incrementar:(id)sender {
    
    contador = contador + 1;
    [muestraNumeros setText:[NSString stringWithFormat:@"%d", contador]];
    
}

- (IBAction)decrementar:(id)sender {
    
    contador = contador - 1;
    [muestraNumeros setText:[NSString stringWithFormat:@"%d", contador]];
    
}

- (IBAction)multiplicar:(id)sender {
    
    contador = contador * 2;
    [muestraNumeros setText:[NSString stringWithFormat:@"%d", contador]];
    
}

- (IBAction)guardarDatos:(id)sender {
    
    NSString *nombre = [NSString stringWithString:campoNombre.text];
    NSString *apellido = [NSString stringWithString:campoApellido.text];
    
    
    PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
    [testObject setObject:nombre forKey:@"nombre"];
    [testObject setObject:apellido forKey:@"apellido"];
    
    [testObject save];
    
    
}
@end













