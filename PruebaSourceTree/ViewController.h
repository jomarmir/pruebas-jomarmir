//
//  ViewController.h
//  PruebaSourceTree
//
//  Created by JMM on 19/01/13.
//  Copyright (c) 2013 JMM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    int contador;
    
}

- (IBAction)incrementar:(id)sender;
- (IBAction)decrementar:(id)sender;
- (IBAction)multiplicar:(id)sender;
- (IBAction)guardarDatos:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *muestraNumeros;
@property (weak, nonatomic) IBOutlet UITextField *campoNombre;
@property (weak, nonatomic) IBOutlet UITextField *campoApellido;



@end
